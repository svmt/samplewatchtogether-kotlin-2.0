package com.wt.sample.data

data class DiagnosticData(
    val diagnosticParents: MutableList<DiagnosticParent>
) {
    data class DiagnosticParent(
        val parent: String,
        val diagnosticChildren: MutableList<DiagnosticChild>
    ) {
        data class DiagnosticChild(
            val status: Boolean,
            val name: String
        )
    }
}