package com.wt.sample.data

class ExpandableDiagnosticModel {

    companion object {
        const val PARENT = 1
        const val CHILD = 2
    }

    var diagnosticParentParent: DiagnosticData.DiagnosticParent? = null
    var diagnosticParentChild: DiagnosticData.DiagnosticParent.DiagnosticChild? = null
    var type: Int
    var isExpanded: Boolean

    private var isCloseShown: Boolean

    constructor(
        type: Int,
        diagnosticParentParent: DiagnosticData.DiagnosticParent,
        isExpanded: Boolean = false,
        isCloseShown: Boolean = false
    ) {
        this.type = type
        this.diagnosticParentParent = diagnosticParentParent
        this.isExpanded = isExpanded
        this.isCloseShown = isCloseShown
    }

    constructor(
        type: Int,
        diagnosticParentChild: DiagnosticData.DiagnosticParent.DiagnosticChild,
        isExpanded: Boolean = false,
        isCloseShown: Boolean = false
    ) {
        this.type = type
        this.diagnosticParentChild = diagnosticParentChild
        this.isExpanded = isExpanded
        this.isCloseShown = isCloseShown
    }
}