package com.wt.sample.data

data class Message(
    val id: String,
    val message: String,
    val name: String,
    val date: Long,
    val isLocalMessage: Boolean
)
