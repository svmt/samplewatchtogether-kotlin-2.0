package com.wt.sample.ui

import android.Manifest.permission.CAMERA
import android.Manifest.permission.RECORD_AUDIO
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Button
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.wt.sample.BuildConfig
import com.wt.sample.R
import com.wt.sample.utill.showError


class StartActivity : Activity() {

    companion object {
        const val DISPLAY_NAME_CODE = "display_name_code"

        const val TYPE_LOGIN = "login"
        const val TYPE_DIAGNOSTIC = "diagnostic"
    }

    private lateinit var mDisplayName: AppCompatEditText

    private var classType: String = TYPE_LOGIN

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        (findViewById<View>(R.id.app_version) as AppCompatTextView).text = BuildConfig.VERSION_NAME

        val sharedPref = getSharedPreferences(
            getString(R.string.preference_file_key),
            Context.MODE_PRIVATE
        )

        mDisplayName = findViewById(R.id.display_name)
        mDisplayName.setText(sharedPref.getString(DISPLAY_NAME_CODE, ""))

        // Click Listener for sign in button
        (findViewById<Button>(R.id.sign_in)).setOnClickListener {
            classType = TYPE_LOGIN
            checkMediaPermissions(TYPE_LOGIN)
        }
        findViewById<View>(R.id.diagnostic).setOnClickListener {
            classType = TYPE_DIAGNOSTIC
            checkMediaPermissions(TYPE_DIAGNOSTIC)
        }

        // Done actions sign in button click
        mDisplayName.setOnEditorActionListener { v, actionId, event ->
            when (actionId) {
                EditorInfo.IME_ACTION_DONE -> {
                    checkMediaPermissions(TYPE_LOGIN)
                    true
                }
                else -> false
            }
        }
    }

    private fun checkMediaPermissions(type: String) {
        if (ContextCompat.checkSelfPermission(this, RECORD_AUDIO) +
            ContextCompat.checkSelfPermission(this, CAMERA) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this, arrayOf(RECORD_AUDIO, CAMERA), 1)
            return
        }
        // Permissions granted
        signIn(type)
    }

    // Check if user granted media permissions for app
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1 && grantResults.size == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[0] == grantResults[1]) {
            signIn(classType)
            return
        }
        // Permissions not allowed
        showError(this, "Camera and Microphone permissions not allowed")
    }

    private fun signIn(type: String) {
        val displayName = mDisplayName.text.toString()
        val sharedPref = getSharedPreferences(
            getString(R.string.preference_file_key),
            Context.MODE_PRIVATE
        ) ?: return

        with(sharedPref.edit()) {
            putString(DISPLAY_NAME_CODE, displayName)
            apply()
        }

        startMainActivity(type, displayName)
    }

    private fun startMainActivity(type: String, displayName: String) {
        val activity = if (type == TYPE_LOGIN) MainActivity::class.java else DiagnosticActivity::class.java
        val intent = Intent(this@StartActivity, activity)
        intent.putExtra(DISPLAY_NAME_CODE, displayName)
        // Start MainActivity with params
        startActivity(intent)
    }

    private fun onError(error: String?) {
        showError(this, error)
    }
}