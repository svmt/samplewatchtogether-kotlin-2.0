package com.wt.sample.ui

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.SeekBar
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.wt.sample.R
import com.wt.sdk.VideoRenderer
import com.wt.sdk.data.participant.*
import com.wt.sdk.params.MediaConfiguration
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class ParticipantsAdapter(
        private val mSwitchCamCallback: SwitchCamCallback,
) : RecyclerView.Adapter<ParticipantsAdapter.ViewHolder>() {

    private val mParticipants = ArrayList<Participant>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        return ViewHolder(
                LayoutInflater.from(viewGroup.context).inflate(
                        R.layout.item_participant,
                        viewGroup,
                        false
                )
        )
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val participant = mParticipants[position]

        participant.setRenderer(viewHolder.videoRenderer)
        participant.enableStats()
        participant.setParticipantStatsListener(object : OnParticipantStatsListener {
            override fun onStats(stats: JSONObject?, participant: Participant) {
                if (stats != null) {
                    updateStats(viewHolder, stats, participant)
                }
            }
        })
        participant.enableRemoteAudioSpeaker()
        participant.setRemoteParticipantSpeakerListener(object : OnRemoteParticipantSpeakerListener {
            override fun onRemoteParticipantStartSpeaking() {
                viewHolder.ivActiveSpeaker.visibility = View.VISIBLE
            }

            override fun onRemoteParticipantStopSpeaking() {
                viewHolder.ivActiveSpeaker.visibility = View.GONE
            }
        })

        viewHolder.mic.setOnClickListener {
            if (participant.isAudioEnabled) {
                participant.disableAudio()
            } else {
                participant.enableAudio()
            }
            notifyItemChanged(position, participant)
        }
        viewHolder.cam.setOnClickListener {
            if (participant.isVideoEnabled) {
                participant.disableVideo()
            } else {
                participant.enableVideo()
            }
            notifyItemChanged(position, participant)
        }
        viewHolder.connectionState.visibility = if (participant.isConnectionLost) View.VISIBLE else View.GONE
        viewHolder.layoutProgress.visibility = if (participant.isProgressReconnection) View.VISIBLE else View.GONE
        viewHolder.mic.setBackgroundResource(if (participant.isAudioEnabled) R.drawable.ic_mic_on else R.drawable.ic_mic_off)
        viewHolder.cam.setBackgroundResource(if (participant.isVideoEnabled) R.drawable.ic_video_on else R.drawable.ic_video_off)

        val participantTypeRes: Int
        when (participant.participantType) {
            ParticipantType.VIEWER -> {
                viewHolder.videoRenderer.clearImage()
                participantTypeRes = R.drawable.ic_viewer
                viewHolder.ivParticipantType.visibility = View.VISIBLE
            }
            ParticipantType.A_BROADCASTER -> {
                viewHolder.videoRenderer.clearImage()
                participantTypeRes = R.drawable.ic_microphone
                viewHolder.ivParticipantType.visibility = View.VISIBLE
            }
            ParticipantType.AV_BROADCASTER -> {
                viewHolder.ivParticipantType.visibility = View.GONE
                participantTypeRes = 0
            }
            ParticipantType.FULL_PARTICIPANT -> {
                participantTypeRes = 0
                viewHolder.ivParticipantType.visibility = View.GONE
            } else -> {
            participantTypeRes = 0
            viewHolder.ivParticipantType.visibility = View.GONE
        }
        }
        viewHolder.ivParticipantType.setImageResource(participantTypeRes)

        viewHolder.name.text = participant.name

        // Check if its local participant
        viewHolder.switchCam.visibility = if (participant is LocalParticipant) View.VISIBLE else View.GONE
        viewHolder.volumeLevel.visibility = if (participant is LocalParticipant) View.GONE else View.VISIBLE

        // Check if its local participant
        if (position == 0) {
            viewHolder.switchCam.visibility = View.VISIBLE
            viewHolder.volumeLevel.visibility = View.GONE
            viewHolder.switchCam.setOnClickListener {
                mSwitchCamCallback.onSwitchCameraClicked()
            }
        } else {
            viewHolder.volumeLevel.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(
                        seekBar: SeekBar,
                        progress: Int,
                        fromUser: Boolean,
                ) {
                    if (fromUser) {
                        /**
                         * Sets the volume for the underlying MediaSource. Volume is a gain value in the range
                         * 0 to 10.
                         */
                        participant.setVolumeLevel(progress)
                    }
                }

                override fun onStartTrackingTouch(seekBar: SeekBar) {}
                override fun onStopTrackingTouch(seekBar: SeekBar) {}
            })
        }
    }

    override fun getItemCount(): Int {
        return mParticipants.size
    }

    fun remoteParticipantConnectionLost(participantId: String?) {
        for (i in mParticipants.indices) {
            val participant = mParticipants[i]
            if (participant.id == participantId) {
                val itemIndex = mParticipants.indexOf(participant)
                participant.isConnectionLost = true
                notifyItemChanged(itemIndex, participant)
            }
        }
    }

    fun updateParticipant(participantId: String, participant: Participant) {
        if (!isExistParticipant(participantId)) {
            addRemoteParticipant(participant)
            return
        }
        for (index in mParticipants.indices) {
            val participantExist = mParticipants[index]
            if (participantExist.id == participantId) {
                participantExist.update(participant)
                notifyItemChanged(index, participantExist)
                break
            }
        }
    }

    fun progressConnection(participantId: String?, isProgress: Boolean) {
        mParticipants.forEachIndexed { index, participant ->
            if (participant.id == participantId || participant.oldParticipantId == participantId) {
                participant.isProgressReconnection = isProgress
                mParticipants[index] = participant
                notifyItemChanged(index, participant)
            }
        }
    }

    fun addLocalParticipant(participant: Participant) {
        if (mParticipants.size > 0) {
            mParticipants[0].update(participant)
            notifyItemChanged(0, participant)
        } else {
            mParticipants.add(participant)
            notifyItemInserted(itemCount - 1)
        }
    }

    fun addRemoteParticipant(participant: Participant) {
        mParticipants.add(participant)
        notifyItemInserted(itemCount - 1)
    }

    fun removeParticipant(participantId: String) {
        for (index in mParticipants.indices) {
            val participant = mParticipants[index]
            if (participant.id == participantId) {
                participant.dispose()
                mParticipants.remove(participant)
                notifyItemRemoved(index)
                break
            }
        }
    }

    fun clearParticipants() {
        mParticipants.forEach { participant -> participant.dispose() }
        mParticipants.clear()
        notifyDataSetChanged()
    }

    fun updateParticipantMedia(
            participantId: String,
            mediaType: MediaConfiguration.MediaType?,
            mediaState: MediaConfiguration.MediaState?
    ) {
        if (mParticipants.isNotEmpty()) {
            mParticipants.forEachIndexed { index, participant ->
                if (participant.id == participantId) {
                    val isEnableMediaState = mediaState == MediaConfiguration.MediaState.ENABLED
                    when (mediaType) {
                        MediaConfiguration.MediaType.AUDIO -> participant.isAudioEnabled = isEnableMediaState
                        MediaConfiguration.MediaType.VIDEO -> participant.isVideoEnabled = isEnableMediaState
                        else -> {
                        }
                    }
                    // Update exactly view after its changed
                    notifyItemChanged(index, participant)
                    return@forEachIndexed
                }
            }
        }
    }

    private fun isExistParticipant(participantId: String): Boolean {
        var isExistParticipant = false
        mParticipants.forEach{ participant ->
            if (participant.oldParticipantId == participantId || participant.id == participantId) {
                isExistParticipant = true
            }
        }
        return isExistParticipant
    }

    fun getParticipantName(participantId: String): String {
        mParticipants.forEach { participant ->
            if (participant.id == participantId) {
                return participant.name
            }
        }
        return ""
    }

    private fun updateStats(
        viewHolder: ViewHolder,
        jsonStats: JSONObject?,
        participant: Participant
    ) {
        val strVideoStats = "${jsonStats?.get("videoAvg")} -> ${jsonStats?.get("videoQosMos")}"
        val strAudioStats = "${jsonStats?.get("audioAvg")} -> ${jsonStats?.get("audioQosMos")}"
        val connectionId = "${jsonStats?.get("connectionId")}"
        Log.i(
            "ParticipantsAdapter",
            "strVideoStats=$strVideoStats, " +
                    "strAudioStats=$strAudioStats, " +
                    "connectionId=${connectionId}"
        )
        val micStateResources = when {
            jsonStats?.get("audioQosMos").toString() == "EXCELLENT" -> {
                if (participant.isAudioEnabled) R.drawable.ic_audio_mos_excellent
                else R.drawable.ic_audio_off_mos_excellent
            }
            jsonStats?.get("audioQosMos").toString() == "GOOD" -> {
                if (participant.isAudioEnabled) R.drawable.ic_audio_mos_good
                else R.drawable.ic_audio_off_mos_good
            }
            else -> {
                if (participant.isAudioEnabled) R.drawable.ic_audio_mos_bad
                else R.drawable.ic_audio_off_mos_bad
            }
        }
        viewHolder.mic.setBackgroundResource(micStateResources)

        val camStateResources = when {
            jsonStats?.get("videoQosMos").toString() == "EXCELLENT" -> {
                if (participant.isVideoEnabled) R.drawable.ic_video_mos_excellent
                else R.drawable.ic_video_off_mos_excellent
            }
            jsonStats?.get("videoQosMos").toString() == "GOOD" -> {
                if (participant.isVideoEnabled) R.drawable.ic_video_mos_good
                else R.drawable.ic_video_off_mos_good
            }
            else -> {
                if (participant.isVideoEnabled) R.drawable.ic_video_mos_bad
                else R.drawable.ic_video_off_mos_bad
            }
        }
        viewHolder.cam.setBackgroundResource(camStateResources)
    }

    interface SwitchCamCallback {
        fun onSwitchCameraClicked()
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val layoutProgress: LinearLayout = v.findViewById(R.id.layout_progress)
        val ivParticipantType: AppCompatImageView = v.findViewById(R.id.iv_participant_type)
        val videoRenderer: VideoRenderer = v.findViewById(R.id.video_renderer)
        val ivActiveSpeaker: AppCompatImageView = v.findViewById(R.id.iv_active_speaker)
        val name: AppCompatTextView = v.findViewById(R.id.participant_name)
        val connectionState: AppCompatTextView = v.findViewById(R.id.txt_connection_state)
        val mic: AppCompatImageView = v.findViewById(R.id.mic)
        val cam: AppCompatImageView = v.findViewById(R.id.cam)
        val switchCam: AppCompatImageView = v.findViewById(R.id.switch_cam)
        val volumeLevel: SeekBar = v.findViewById(R.id.volume_level)
    }
}