package com.wt.sample.ui

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.wt.sample.R
import com.wt.sample.data.Message
import com.wt.sample.utill.formatDurationToHHMMSSSSS
import com.wt.sample.utill.showError
import com.wt.sample.utill.showMessage
import com.wt.sdk.*
import com.wt.sdk.data.participant.Participant
import com.wt.sdk.data.participant.ParticipantType
import com.wt.sdk.params.MediaConfiguration
import java.util.*

class MainActivity : AppCompatActivity(), SessionListener, SessionReconnectListener,
    SessionConnectionListener,
    ParticipantsAdapter.SwitchCamCallback {

    // Const string keys to store values
    companion object {
        const val SESSION_CONNECTION_KEY = "session_connection_key"
        const val PARTICIPANT_TYPE_KEY = "participant_type"
    }

    // Bundle object to save and get values
    private val mBundle = Bundle()

    private var mSession: Session? = null
    private var mParticipantAdapter: ParticipantsAdapter? = null
    private var mDisplayName = ""
    private var mToken = "PUT_TOKEN"
    private var mReconnectTimer: Timer? = null
    private var mCountTimer = Timer("MainActivityCountThread")
    private var mConnectionStateTimer = Timer("MainActivityConnectionThread")

    private lateinit var mConnectionState: AppCompatTextView
    private lateinit var mParticipantsTypesSheet: LinearLayout

    private val mMessengerAdapter: MessengerAdapter = MessengerAdapter()
    private var mRecyclerMessages: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // To prevent screen to be switched off
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        // Extract session's params
        mDisplayName = intent.extras?.get(StartActivity.DISPLAY_NAME_CODE).toString()

        initViewsAndListeners()
        initSession()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        (findViewById<RecyclerView>(R.id.participants).layoutManager as GridLayoutManager).spanCount =
            if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) 2 else 4
    }

    private fun initViewsAndListeners() {
        val btnStartCameraPreview = findViewById<AppCompatButton>(R.id.btn_cam_preview)
        val btnConnectSessionFP = findViewById<AppCompatButton>(R.id.btn_connect_session_fp)
        val btnConnectSessionAVB = findViewById<AppCompatButton>(R.id.btn_connect_session_avb)
        val btnConnectSessionAB = findViewById<AppCompatButton>(R.id.btn_connect_session_ab)
        val btnConnectWatching = findViewById<AppCompatButton>(R.id.btn_connect_watching)
        val btnAdjustVideoQuality = findViewById<AppCompatButton>(R.id.btn_adjust_frame_rate)
        val btnFPType = findViewById<AppCompatButton>(R.id.btn_pt_fp)
        val btnAVBType = findViewById<AppCompatButton>(R.id.btn_pt_avb)
        val btnABType = findViewById<AppCompatButton>(R.id.btn_pt_ab)
        val btnViewerType = findViewById<AppCompatButton>(R.id.btn_pt_v)
        val txtWidth = findViewById<AppCompatTextView>(R.id.txt_width_value)
        val txtHeight = findViewById<AppCompatTextView>(R.id.txt_height_value)
        val txtFrameRate = findViewById<AppCompatTextView>(R.id.txt_frame_rate_value)
        mParticipantsTypesSheet = this.findViewById(R.id.participants_types_sheet)
        mConnectionState = findViewById(R.id.txt_connection_state)
        val widthSeek = findViewById<SeekBar>(R.id.seek_width)
        val heightSeek = findViewById<SeekBar>(R.id.seek_height)
        val frameSeek = findViewById<SeekBar>(R.id.seek_frame_rate)
        setParticipantsTypesSheet()
        txtWidth.text = widthSeek.progress.toString()
        txtHeight.text = heightSeek.progress.toString()
        txtFrameRate.text = frameSeek.progress.toString()
        // chat messages views
        val editMessage = findViewById<AppCompatEditText>(R.id.edit_message)
        val btnSendMessage = findViewById<AppCompatImageView>(R.id.btn_send_message)
        mRecyclerMessages = findViewById(R.id.recycler_messages)
        mRecyclerMessages?.adapter = mMessengerAdapter

        btnStartCameraPreview.setOnClickListener {
            btnStartCameraPreview.visibility = View.GONE
            findViewById<View>(R.id.adjust_media_quality_container).visibility = View.VISIBLE
            mSession?.setVideoResolution(widthSeek.progress, heightSeek.progress)
            mSession?.setVideoFrameRate(frameSeek.progress)
            mSession?.startCameraPreview()
        }

        btnConnectSessionFP.setOnClickListener {
            findViewById<View>(R.id.adjust_media_quality_container).visibility = View.VISIBLE
            findViewById<View>(R.id.controls).visibility = View.GONE
            setSessionParams()
            mSession?.setVideoResolution(widthSeek.progress, heightSeek.progress)
            mSession?.setVideoFrameRate(frameSeek.progress)
            mSession?.connect(mToken)
            mBundle.putString(PARTICIPANT_TYPE_KEY, ParticipantType.FULL_PARTICIPANT.name)
        }

        btnConnectSessionAVB.setOnClickListener {
            findViewById<View>(R.id.adjust_media_quality_container).visibility = View.VISIBLE
            findViewById<View>(R.id.controls).visibility = View.GONE
            setSessionParams()
            mSession?.setVideoResolution(widthSeek.progress, heightSeek.progress)
            mSession?.setVideoFrameRate(frameSeek.progress)
            mSession?.connect(mToken, ParticipantType.AV_BROADCASTER)
            mBundle.putString(PARTICIPANT_TYPE_KEY, ParticipantType.AV_BROADCASTER.name)
        }

        btnConnectSessionAB.setOnClickListener {
            findViewById<View>(R.id.adjust_media_quality_container).visibility = View.VISIBLE
            findViewById<View>(R.id.controls).visibility = View.GONE
            setSessionParams()
            mSession?.connect(mToken, ParticipantType.A_BROADCASTER)
            mBundle.putString(PARTICIPANT_TYPE_KEY, ParticipantType.A_BROADCASTER.name)
        }

        btnConnectWatching.setOnClickListener {
            findViewById<View>(R.id.adjust_media_quality_container).visibility = View.VISIBLE
            findViewById<View>(R.id.controls).visibility = View.GONE
            setSessionParams()
            mSession?.connect(mToken, ParticipantType.VIEWER)
            mBundle.putString(PARTICIPANT_TYPE_KEY, ParticipantType.VIEWER.name)
        }

        btnFPType.setOnClickListener {
            mSession?.setLocalParticipantType(ParticipantType.FULL_PARTICIPANT)
            mBundle.putString(PARTICIPANT_TYPE_KEY, ParticipantType.FULL_PARTICIPANT.name)
        }

        btnAVBType.setOnClickListener {
            mSession?.setLocalParticipantType(ParticipantType.AV_BROADCASTER)
            mBundle.putString(PARTICIPANT_TYPE_KEY, ParticipantType.AV_BROADCASTER.name)
        }

        btnABType.setOnClickListener {
            mSession?.setLocalParticipantType(ParticipantType.A_BROADCASTER)
            mBundle.putString(PARTICIPANT_TYPE_KEY, ParticipantType.A_BROADCASTER.name)
        }

        btnViewerType.setOnClickListener {
            mSession?.setLocalParticipantType(ParticipantType.VIEWER)
            mBundle.putString(PARTICIPANT_TYPE_KEY, ParticipantType.VIEWER.name)
        }

        btnAdjustVideoQuality.setOnClickListener {
            mSession?.adjustResolution(widthSeek.progress, heightSeek.progress)
            mSession?.adjustFrameRate(frameSeek.progress)
        }

        btnSendMessage.setOnClickListener {
            val message = editMessage.text
            mMessengerAdapter.addMessage(
                Message(
                    System.currentTimeMillis().toString(),
                    message.toString(),
                    mDisplayName,
                    System.currentTimeMillis(),
                    true
                )
            )
            mSession?.sendMessage(message.toString())
            message?.clear()
            mRecyclerMessages?.scrollToPosition(mMessengerAdapter.itemCount - 1)
        }

        widthSeek.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(
                seekBar: SeekBar,
                progress: Int,
                fromUser: Boolean
            ) {
                if (fromUser) {
                    val widthProgress = getResolutionProgress(progress)
                    widthSeek.progress = widthProgress
                    txtWidth.text = widthProgress.toString()
                    val heightProgress =
                        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
                            getResolutionProgress(widthProgress * 4 / 3)
                        } else {
                            getResolutionProgress(widthProgress * 3 / 4)
                        }
                    heightSeek.progress = heightProgress
                    txtHeight.text = heightProgress.toString()
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        heightSeek.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(
                seekBar: SeekBar,
                progress: Int,
                fromUser: Boolean
            ) {
                if (fromUser) {
                    val heightProgress = getResolutionProgress(progress)
                    heightSeek.progress = heightProgress
                    txtHeight.text = heightProgress.toString()
                    val widthProgress =
                        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
                            getResolutionProgress(heightProgress * 3 / 4)
                        } else {
                            getResolutionProgress(heightProgress * 4 / 3)
                        }
                    widthSeek.progress = widthProgress
                    txtWidth.text = widthProgress.toString()
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        frameSeek.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(
                seekBar: SeekBar,
                progress: Int,
                fromUser: Boolean
            ) {
                if (fromUser) {
                    txtFrameRate.text = progress.toString()
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        // Create ParticipantsAdapter instance
        mParticipantAdapter = ParticipantsAdapter(this)
        // Set ParticipantsAdapter to RecyclerView
        val recycler = findViewById<RecyclerView>(R.id.participants)
        recycler.adapter = mParticipantAdapter
        (recycler.layoutManager as GridLayoutManager).spanCount =
            if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) 2 else 4
    }

    private fun getResolutionProgress(progress: Int): Int {
        return when {
            progress < 144 -> 120
            progress < 160 -> 144
            progress < 176 -> 160
            progress < 240 -> 176
            progress < 288 -> 240
            progress < 320 -> 288
            progress < 352 -> 320
            progress < 360 -> 352
            progress < 480 -> 360
            progress < 600 -> 480
            progress < 640 -> 600
            progress < 720 -> 640
            progress < 768 -> 720
            progress < 800 -> 768
            progress < 960 -> 800
            progress < 1024 -> 960
            progress < 1080 -> 1024
            progress < 1200 -> 1080
            progress < 1280 -> 1200
            progress < 1440 -> 1280
            progress < 1512 -> 1440
            progress < 1536 -> 1512
            progress < 1600 -> 1536
            progress < 1944 -> 1600
            progress < 2400 -> 1944
            progress < 2448 -> 2400
            progress < 2560 -> 2448
            progress < 3264 -> 2560
            else -> 3264
        }
    }

    private fun setParticipantsTypesSheet() {
        BottomSheetBehavior.from(mParticipantsTypesSheet).state = BottomSheetBehavior.STATE_EXPANDED
        BottomSheetBehavior.from(mParticipantsTypesSheet).isDraggable = true

        //click event for show-dismiss bottom sheet
        findViewById<View>(R.id.sheet_header).setOnClickListener {
            if (BottomSheetBehavior.from(mParticipantsTypesSheet).state != BottomSheetBehavior.STATE_EXPANDED) {
                BottomSheetBehavior.from(mParticipantsTypesSheet)
                    .setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                BottomSheetBehavior.from(mParticipantsTypesSheet)
                    .setState(BottomSheetBehavior.STATE_COLLAPSED)
            }
        }
    }

    private fun initSession() {
        mSession = Session.SessionBuilder(this)
            .setReconnectListener(this)
            .setConnectionListener(this)
            .build(applicationContext)
    }

    private fun setSessionParams() {
        mSession?.displayName = mDisplayName
    }

    override fun onPause() {
        disconnect()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        if (mBundle.getBoolean(SESSION_CONNECTION_KEY, false)) {
            mBundle.putBoolean(SESSION_CONNECTION_KEY, false)
            initSession()
            mSession?.connect(
                mToken,
                ParticipantType.valueOf(mBundle.getString(PARTICIPANT_TYPE_KEY, "FULL_PARTICIPANT"))
            )
        }
    }

    override fun onBackPressed() {
        mBundle.putBoolean(SESSION_CONNECTION_KEY, false)
        mSession?.resetLocalAudioVideoState()
        super.onBackPressed()
    }

    private fun disconnect() {
        clearAdapters()
        mSession?.disconnect()
        mSession = null
    }

    private fun clearAdapters() {
        // Clear participants and disconnect from the session
        mParticipantAdapter?.clearParticipants()
    }

    override fun onSwitchCameraClicked() {
        mSession?.switchCamera()
    }

    override fun onConnected(participants: List<Participant>) {
        mBundle.putBoolean(SESSION_CONNECTION_KEY, true)
        BottomSheetBehavior.from(mParticipantsTypesSheet).state = BottomSheetBehavior.STATE_COLLAPSED
        findViewById<View>(R.id.type_container).visibility = View.VISIBLE
        findViewById<View>(R.id.txt_session_time_duration).visibility = View.VISIBLE
        findViewById<View>(R.id.message_container).visibility = View.VISIBLE
        showMessage(this, "Connected to session")
        val connectionStarted = System.currentTimeMillis()
        mCountTimer.schedule(object : TimerTask() {
            override fun run() {
                if (isFinishing) {
                    cancel()
                    return
                }
                runOnUiThread {
                    findViewById<AppCompatTextView>(R.id.txt_session_time_duration).text =
                        formatDurationToHHMMSSSSS(System.currentTimeMillis() - connectionStarted)
                }
            }
        }, 0, 1000)
    }

    override fun onError(error: SessionError) {
        when (error.code) {
            101 -> mSession?.disconnect()
            106 -> mSession?.disconnect()
        }
        showError(this, error.message.toString())
    }

    override fun onForceDisconnected(participantId: String, message: String?) {
        showMessage(applicationContext, "force disconnect Participant with message $message")
        onDisconnected()
        finish()
    }

    override fun onDisconnected() {
        showError(this, "Disconnected from session")
        mSession?.resetLocalAudioVideoState()
        finish()
    }

    override fun onLocalParticipantJoined(participant: Participant) {
        mParticipantAdapter?.addLocalParticipant(participant)
    }

    override fun onRemoteParticipantJoined(participant: Participant) {
        mParticipantAdapter?.addRemoteParticipant(participant)
        // You can set RemoteParticipant's volume level after adding participant's object to adapter
        // Volume is a gain value in the range 0 to 10.
        participant.setVolumeLevel(1)
    }

    override fun onMessageReceived(participantId: String, message: String) {
        val participantName = mParticipantAdapter?.getParticipantName(participantId) ?: ""

        Log.i(
            "MESSAGE",
            "Got message from participant with id->$participantId and name->$participantName, message->$message)"
        )

        mMessengerAdapter.addMessage(
            Message(
                System.currentTimeMillis().toString(),
                message,
                participantName,
                System.currentTimeMillis(),
                false
            )
        )
        mRecyclerMessages?.scrollToPosition(mMessengerAdapter.itemCount - 1)
    }

    override fun onUpdateParticipant(participantId: String, participant: Participant) {
        mParticipantAdapter?.updateParticipant(participantId, participant)
    }

    override fun onRemoteParticipantLeft(participantId: String) {
        mParticipantAdapter?.removeParticipant(participantId)
    }

    override fun onParticipantMediaStateChanged(
        participantId: String,
        mediaType: MediaConfiguration.MediaType,
        mediaState: MediaConfiguration.MediaState
    ) {
        mParticipantAdapter?.updateParticipantMedia(participantId, mediaType, mediaState)
    }

    /**
     * SessionReconnectListener's callbacks
     *
     * */
    override fun onParticipantReconnecting(participantId: String) {
        mParticipantAdapter?.progressConnection(participantId, true)
        mReconnectTimer?.cancel()
        mReconnectTimer = Timer("MainActivityReconnectThread")
        mReconnectTimer?.schedule(object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    mParticipantAdapter?.removeParticipant(participantId)
                }
            }
        }, 7000)
    }

    override fun onParticipantReconnected(participantId: String, oldParticipantId: String) {
        mReconnectTimer?.cancel()
        if (isFinishing) {
            return
        }
        mParticipantAdapter?.progressConnection(oldParticipantId, false)
    }

    /**
     * SessionConnectionListener's callbacks
     *
     * */
    override fun onLocalConnectionLost() {
        setConnectionState(R.string.txt_connection_lost, false)
    }

    override fun onLocalConnectionResumed() {
        setConnectionState(R.string.txt_connection_resumed, true)
    }

    override fun onRemoteConnectionLost(participantId: String) {
        if (isFinishing) {
            return
        }
        mParticipantAdapter?.remoteParticipantConnectionLost(participantId)
        mReconnectTimer?.cancel()
        mReconnectTimer = Timer("MainActivityReconnectThread")
        mReconnectTimer?.schedule(object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    mParticipantAdapter?.removeParticipant(participantId)
                }
            }
        }, 15_000)
    }

    private fun setConnectionState(strConnectionState: Int, isConnected: Boolean) {
        val txtColor = if (isConnected) R.color.colorGreen else R.color.colorRed
        mConnectionState.setText(strConnectionState)
        mConnectionState.setBackgroundColor(ContextCompat.getColor(this, txtColor))
        if (mConnectionState.visibility != View.VISIBLE) {
            mConnectionState.visibility = View.VISIBLE
        }
        if (isConnected) {
            mConnectionStateTimer.schedule(object : TimerTask() {
                override fun run() {
                    runOnUiThread {
                        mConnectionState.visibility = View.GONE
                    }
                }
            }, 3_000)
        }
    }
}