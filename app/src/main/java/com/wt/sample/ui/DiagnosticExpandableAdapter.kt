package com.wt.sample.ui

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.wt.sample.R
import com.wt.sample.data.DiagnosticData
import com.wt.sample.data.ExpandableDiagnosticModel
import org.webrtc.IceCandidate

class DiagnosticExpandableAdapter(
    var mDiagnosticStateModelList: MutableList<ExpandableDiagnosticModel>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ExpandableDiagnosticModel.PARENT -> {
                ParentViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.item_expandable_parent, parent, false
                    )
                )
            }
            ExpandableDiagnosticModel.CHILD -> {
                ChildViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.item_expandable_child, parent, false
                    )
                )
            }
            else -> {
                ParentViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.item_expandable_parent, parent, false
                    )
                )
            }
        }
    }

    override fun getItemCount(): Int = mDiagnosticStateModelList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val row = mDiagnosticStateModelList[position]
        when (row.type) {
            ExpandableDiagnosticModel.PARENT -> {
                (holder as ParentViewHolder).diagnosticName.text =
                    row.diagnosticParentParent?.parent

                holder.parentLayout.setOnClickListener {
                    if (row.isExpanded) collapseRow(position) else expandRow(position)
                    row.isExpanded = !row.isExpanded
//                    val backgroundColor = if (row.isExpanded) Color.GRAY else Color.TRANSPARENT
//                    holder.parentLayout.setBackgroundColor(backgroundColor)
                    holder.closeImage.visibility = if (row.isExpanded) View.GONE else View.VISIBLE
                    holder.upArrowImg.visibility = if (row.isExpanded) View.VISIBLE else View.GONE
                }
            }
            ExpandableDiagnosticModel.CHILD -> {
                (holder as ChildViewHolder).stateName.text = row.diagnosticParentChild?.name
                val statusDrawable =
                    if (row.diagnosticParentChild?.status == true) R.drawable.ic_done else R.drawable.ic_failed
                holder.capitalImage.setBackgroundResource(statusDrawable)
            }
        }

    }

    override fun getItemViewType(position: Int): Int = mDiagnosticStateModelList[position].type

    fun addDiagnosticChildItemData(childItemData: ExpandableDiagnosticModel) {
        mDiagnosticStateModelList.add(childItemData)
        notifyItemInserted(itemCount - 1)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun clearAdapter() {
        mDiagnosticStateModelList.clear()
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun expandRow(position: Int) {
        val row = mDiagnosticStateModelList[position]
        var nextPosition = position
        when (row.type) {
            ExpandableDiagnosticModel.PARENT -> {
                val diagnosticChild: MutableList<DiagnosticData.DiagnosticParent.DiagnosticChild>? =
                    row.diagnosticParentParent?.diagnosticChildren
                if (diagnosticChild != null) {
                    for (child in diagnosticChild) {
                        mDiagnosticStateModelList.add(
                            ++nextPosition,
                            ExpandableDiagnosticModel(ExpandableDiagnosticModel.CHILD, child)
                        )
                    }
                    notifyDataSetChanged()
                }
            }
            ExpandableDiagnosticModel.CHILD -> {
                notifyDataSetChanged()
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun collapseRow(position: Int) {
        val row = mDiagnosticStateModelList[position]
        val nextPosition = position + 1
        when (row.type) {
            ExpandableDiagnosticModel.PARENT -> {
                while (true) {
                    if (nextPosition == mDiagnosticStateModelList.size ||
                        mDiagnosticStateModelList[nextPosition].type == ExpandableDiagnosticModel.PARENT) {
                        break
                    }
                    mDiagnosticStateModelList.removeAt(nextPosition)
                }
                notifyDataSetChanged()
            }
        }
    }

    class ParentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var parentLayout: ConstraintLayout = itemView.findViewById(R.id.diagnostic_item_parent_container)
        var diagnosticName: AppCompatTextView = itemView.findViewById(R.id.diagnostic_name)
        var closeImage: AppCompatImageView = itemView.findViewById(R.id.close_arrow)
        var upArrowImg: AppCompatImageView = itemView.findViewById(R.id.up_arrow)
    }

    class ChildViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var childLayout: ConstraintLayout = itemView.findViewById(R.id.diagnostic_item_child_container)
        internal var stateName: AppCompatTextView = itemView.findViewById(R.id.txt_diagnostic_name)
        internal var capitalImage: AppCompatImageView = itemView.findViewById(R.id.iv_status_diagnostic)
    }
}