package com.wt.sample.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.wt.sample.R
import com.wt.sample.data.Message
import com.wt.sample.utill.getDate

class MessengerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var mMessages: ArrayList<Message> = arrayListOf()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {

        return if (viewType == 1) {
            MessengerLocalViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_messenger_local,
                    parent,
                    false
                )
            )
        } else {
            MessengerRemoteViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_messenger_remote,
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val message = mMessages[position]
        val date = getDate(message.date)

        if (holder is MessengerLocalViewHolder) {
            holder.name.text = message.name
            holder.message.text = message.message
            holder.date.text = date
        } else if (holder is MessengerRemoteViewHolder) {
            holder.name.text = message.name
            holder.message.text = message.message
            holder.date.text = date
        }
    }

    override fun getItemCount(): Int {
        return mMessages.size
    }

    override fun getItemViewType(position: Int): Int {
        val message = mMessages[position]
        return if (message.isLocalMessage) { 1 } else { 2 }
    }

    fun addMessage(message: Message) {
        mMessages.add(message)
        notifyItemInserted(mMessages.size - 1)
    }

    class MessengerLocalViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val name: AppCompatTextView = v.findViewById(R.id.txt_local_name)
        val message: AppCompatTextView = v.findViewById(R.id.txt_local_message)
        val date: AppCompatTextView = v.findViewById(R.id.txt_local_date)
    }

    class MessengerRemoteViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val name: AppCompatTextView = v.findViewById(R.id.txt_remote_name)
        val message: AppCompatTextView = v.findViewById(R.id.txt_remote_message)
        val date: AppCompatTextView = v.findViewById(R.id.txt_remote_date)
    }
}