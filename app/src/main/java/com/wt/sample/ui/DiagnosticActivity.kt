package com.wt.sample.ui

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.ContentLoadingProgressBar
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.wt.sample.R
import com.wt.sample.data.DiagnosticData
import com.wt.sample.data.ExpandableDiagnosticModel
import com.wt.sample.utill.checkVideoResolutions
import com.wt.sdk.diagnostic.SessionDiagnostic
import com.wt.sdk.diagnostic.SessionDiagnosticListener
import org.webrtc.IceCandidate
import org.webrtc.PeerConnection

private const val FILE_URL = "PUT_FILE_URL"

private const val NETWORK_TYPE_UDP: String = "udp"
private const val NETWORK_TYPE_TCP: String = "tcp"
private const val NETWORK_TYPE_IPV6: String = "ipv6"
private const val CONNECTIVITY_TYPE_HOST: String = "host"
private const val CONNECTIVITY_TYPE_RELAY: String = "relay"
private const val CONNECTIVITY_TYPE_REFLEXIVE: String = "srflx"

class DiagnosticActivity : AppCompatActivity(), SessionDiagnosticListener {

    companion object {
        const val PARENT_MICROPHONE = "Microphone"
        const val PARENT_CAMERA = "Camera"
        const val PARENT_CONNECTIVITY = "Connectivity"
        const val PARENT_THROUGHPUT = "Throughput"
        const val PARENT_NETWORK = "Network"
        const val PARENT_ICE_CANDIDATES_GENERATED = "Ice Candidates Generated"
        const val PARENT_ICE_CANDIDATES_RECEIVED = "Ice Candidates Received"
        const val PARENT_PEER_CONNECTION_STATES = "Peer Connection States"
    }

    // Session diagnostic
    private var mSessionDiagnostic: SessionDiagnostic? = null
    private var mDisplayName = ""
    private var mToken = "PUT_TOKEN"
    private var mSessionId = ""

    // diagnostic lists
    private val mExpandableDiagnosticList: MutableList<ExpandableDiagnosticModel> = arrayListOf()
    private val mDiagnosticIceCandidateGenerated: MutableList<DiagnosticData.DiagnosticParent.DiagnosticChild> =
        arrayListOf()
    private val mDiagnosticIceCandidateReceived: MutableList<DiagnosticData.DiagnosticParent.DiagnosticChild> =
        arrayListOf()
    private val mDiagnosticNetwork: MutableList<DiagnosticData.DiagnosticParent.DiagnosticChild> =
        arrayListOf()
    private val mDiagnosticPeerConnectionStates: MutableList<DiagnosticData.DiagnosticParent.DiagnosticChild> =
        arrayListOf()

    private var mDiagnosticExpandableAdapter: DiagnosticExpandableAdapter? = null
    private lateinit var mRecyclerDiagnostic: RecyclerView
    private lateinit var mProgressDiagnostic: ContentLoadingProgressBar

    private val mHandler by lazy { Handler(Looper.getMainLooper()) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_diagnostic)

        // Extract session's params
        mDisplayName = intent.extras?.get(StartActivity.DISPLAY_NAME_CODE).toString()

        initSessionDiagnostic()
        initRecyclerDiagnostic(mExpandableDiagnosticList)
    }

    override fun onPause() {
        super.onPause()
        mSessionDiagnostic?.onUnregister()
    }

    override fun onResume() {
        super.onResume()
        mSessionDiagnostic?.onRegister()
    }

    override fun onDestroy() {
        super.onDestroy()
        mSessionDiagnostic?.onUnregister()
        mSessionDiagnostic?.disconnect()
        mDiagnosticExpandableAdapter?.clearAdapter()
    }

    private fun initRecyclerDiagnostic(expandableDiagnosticList: MutableList<ExpandableDiagnosticModel>) {
        mRecyclerDiagnostic = findViewById(R.id.recycle_diagnostic)
        mProgressDiagnostic = findViewById(R.id.progress_diagnostic)
        mDiagnosticExpandableAdapter = DiagnosticExpandableAdapter(expandableDiagnosticList)
        mDiagnosticExpandableAdapter?.let {
            val layoutManager = LinearLayoutManager(applicationContext)
            mRecyclerDiagnostic.layoutManager = layoutManager
            mRecyclerDiagnostic.adapter = it
            mRecyclerDiagnostic.addItemDecoration(
                DividerItemDecoration(
                    applicationContext,
                    DividerItemDecoration.VERTICAL
                )
            )
        }
    }

    private fun initSessionDiagnostic() {
        mSessionDiagnostic = SessionDiagnostic.SessionDiagnosticBuilder()
            .setToken(mToken)
            .setSessionDiagnosticListener(this)
            .build(applicationContext)

        mSessionDiagnostic?.checkThroughput(FILE_URL)
        mSessionDiagnostic?.checkNetworkType()
        mSessionDiagnostic?.connect()
    }

    override fun onWebSocketConnected(isConnected: Boolean) {
        mHandler.post {
            checkAudio()
            checkVideo()
        }
    }

    override fun onConnected(sessionId: String) {
        mSessionId = sessionId
    }

    override fun onNetworkType(networkType: String) {
        mDiagnosticNetwork.add(
            DiagnosticData.DiagnosticParent.DiagnosticChild(
                true,
                "Network -> $networkType"
            )
        )
    }

    override fun onPeerConnectionState(id: String, state: PeerConnection.PeerConnectionState) {
        mDiagnosticPeerConnectionStates.add(
            DiagnosticData.DiagnosticParent.DiagnosticChild(
                true,
                "State -> $state"
            )
        )

        if (state == PeerConnection.PeerConnectionState.CONNECTED) {
            mDiagnosticExpandableAdapter?.addDiagnosticChildItemData(
                ExpandableDiagnosticModel(
                    ExpandableDiagnosticModel.PARENT,
                    DiagnosticData.DiagnosticParent(
                        PARENT_PEER_CONNECTION_STATES,
                        mDiagnosticPeerConnectionStates
                    ),
                    isExpanded = false,
                    isCloseShown = true
                )
            )
        }
    }

    override fun onThroughputData(result: Boolean, bandwidthQuality: String, bitsPerSecond: String) {
        val diagnosticThroughput: MutableList<DiagnosticData.DiagnosticParent.DiagnosticChild> =
            arrayListOf()

        diagnosticThroughput.add(
            DiagnosticData.DiagnosticParent.DiagnosticChild(
                result,
                "Bandwidth quality $bandwidthQuality"
            )
        )
        diagnosticThroughput.add(
            DiagnosticData.DiagnosticParent.DiagnosticChild(
                result,
                "Bandwidth bits per second $bitsPerSecond"
            )
        )

        mDiagnosticExpandableAdapter?.addDiagnosticChildItemData(
            ExpandableDiagnosticModel(
                ExpandableDiagnosticModel.PARENT,
                DiagnosticData.DiagnosticParent(
                    PARENT_THROUGHPUT,
                    diagnosticThroughput
                ),
                isExpanded = false,
                isCloseShown = true
            )
        )

        mHandler.postDelayed({
            checkIceCandidate()
            checkNetwork()
            checkConnectivity()
        }, 2000)
    }

    override fun onIceCandidateGenerated(candidate: IceCandidate) {
        mDiagnosticIceCandidateGenerated.add(DiagnosticData.DiagnosticParent.DiagnosticChild(
            true,
            candidate.toString()
        ))
    }

    override fun onIceCandidateReceived(candidate: IceCandidate) {
        mDiagnosticIceCandidateReceived.add(DiagnosticData.DiagnosticParent.DiagnosticChild(
            true,
            candidate.toString()
        ))
    }

    private fun checkNetwork() {
        val isEnabledUdpProtocol = isEnableNetworkType(NETWORK_TYPE_UDP)
        val isEnabledTcpProtocol = isEnableNetworkType(NETWORK_TYPE_TCP)
        val isEnabledIpv6Protocol = isEnableNetworkType(NETWORK_TYPE_IPV6)

        mDiagnosticNetwork.add(
            DiagnosticData.DiagnosticParent.DiagnosticChild(
                isEnabledUdpProtocol,
                "Udp enabled"
            )
        )

        mDiagnosticNetwork.add(
            DiagnosticData.DiagnosticParent.DiagnosticChild(
                isEnabledTcpProtocol,
                "Tcp enabled"
            )
        )

        mDiagnosticNetwork.add(
            DiagnosticData.DiagnosticParent.DiagnosticChild(
                isEnabledIpv6Protocol,
                "Ipv6 enabled"
            )
        )

        mDiagnosticExpandableAdapter?.addDiagnosticChildItemData(
            ExpandableDiagnosticModel(
                ExpandableDiagnosticModel.PARENT,
                DiagnosticData.DiagnosticParent(PARENT_NETWORK, mDiagnosticNetwork),
                isExpanded = false,
                isCloseShown = true
            )
        )
    }

    private fun checkVideo() {
        val isCamera = mSessionDiagnostic?.isCheckCameraPermission()
        val diagnosticCamera: MutableList<DiagnosticData.DiagnosticParent.DiagnosticChild> =
            arrayListOf()
        diagnosticCamera.add(
            DiagnosticData.DiagnosticParent.DiagnosticChild(
                isCamera == true,
                "Check camera permission"
            )
        )

        val arrayCodec = mSessionDiagnostic?.getVideoCodec()
        if (arrayCodec != null) {
            for (codec in arrayCodec) {
                diagnosticCamera.add(
                    DiagnosticData.DiagnosticParent.DiagnosticChild(
                        true,
                        "Video codec available ${codec.name}"
                    )
                )
            }
        }

        checkVideoResolutions(applicationContext, diagnosticCamera)

        val isCreatedVideoCapture = mSessionDiagnostic?.isCreatedVideoCapture()
        diagnosticCamera.add(
            DiagnosticData.DiagnosticParent.DiagnosticChild(
                isCreatedVideoCapture == true,
                "Video capture"
            )
        )

        mDiagnosticExpandableAdapter?.addDiagnosticChildItemData(
            ExpandableDiagnosticModel(
                ExpandableDiagnosticModel.PARENT,
                DiagnosticData.DiagnosticParent(PARENT_CAMERA, diagnosticCamera),
                isExpanded = false,
                isCloseShown = false
            )
        )
    }

    private fun checkAudio() {
        val diagnosticChildren: MutableList<DiagnosticData.DiagnosticParent.DiagnosticChild> =
            arrayListOf()

        val isRecordAudio = mSessionDiagnostic?.isCheckRecordAudioPermission()
        diagnosticChildren.add(
            DiagnosticData.DiagnosticParent.DiagnosticChild(
                isRecordAudio == true,
                "Check record audio permission"
            )
        )

        val isCreatedAudioTrack = mSessionDiagnostic?.isCreatedAudioTrack()
        diagnosticChildren.add(
            DiagnosticData.DiagnosticParent.DiagnosticChild(
                isCreatedAudioTrack == true,
                "Audio track created using device"
            )
        )

        mDiagnosticExpandableAdapter?.addDiagnosticChildItemData(
            ExpandableDiagnosticModel(
                ExpandableDiagnosticModel.PARENT,
                DiagnosticData.DiagnosticParent(PARENT_MICROPHONE, diagnosticChildren),
                isExpanded = false,
                isCloseShown = true
            )
        )
    }

    private fun checkConnectivity() {
        val diagnosticConnectivity: MutableList<DiagnosticData.DiagnosticParent.DiagnosticChild> =
            arrayListOf()

        val isHostConnectivity = isEnableNetworkType(CONNECTIVITY_TYPE_HOST)
        val isRelayConnectivity = isEnableNetworkType(CONNECTIVITY_TYPE_RELAY)
        val isReflexiveConnectivity = isEnableNetworkType(CONNECTIVITY_TYPE_REFLEXIVE)

        diagnosticConnectivity.add(
            DiagnosticData.DiagnosticParent.DiagnosticChild(
                isHostConnectivity,
                "Host connectivity"
            )
        )
        diagnosticConnectivity.add(
            DiagnosticData.DiagnosticParent.DiagnosticChild(
                isRelayConnectivity,
                "Relay connectivity"
            )
        )
        diagnosticConnectivity.add(
            DiagnosticData.DiagnosticParent.DiagnosticChild(
                isReflexiveConnectivity,
                "Reflexive connectivity"
            )
        )

        mDiagnosticExpandableAdapter?.addDiagnosticChildItemData(
            ExpandableDiagnosticModel(
                ExpandableDiagnosticModel.PARENT,
                DiagnosticData.DiagnosticParent(
                    PARENT_CONNECTIVITY,
                    diagnosticConnectivity
                ),
                isExpanded = false,
                isCloseShown = true
            )
        )

        mProgressDiagnostic.visibility = View.GONE
        mSessionDiagnostic?.clearVideoCapture()
    }

    private fun isEnableNetworkType(type: String): Boolean {
        for (iceCandidate in mDiagnosticIceCandidateGenerated) {
            if (iceCandidate.name.contains(type)) {
                return true
            }
        }
        return false
    }

    private fun checkIceCandidate() {
        mDiagnosticIceCandidateGenerated.sortedBy { when {
            it.name.contains("typ host") -> 1
            it.name.contains("typ relay") -> 2
            it.name.contains("typ srflx") -> 3
            else -> 4
        }}

        mDiagnosticExpandableAdapter?.addDiagnosticChildItemData(
            ExpandableDiagnosticModel(
                ExpandableDiagnosticModel.PARENT,
                DiagnosticData.DiagnosticParent(
                    PARENT_ICE_CANDIDATES_GENERATED,
                    mDiagnosticIceCandidateGenerated
                ),
                isExpanded = false,
                isCloseShown = true
            )
        )

        mDiagnosticIceCandidateReceived.sortedBy { when {
            it.name.contains("typ host") -> 1
            it.name.contains("typ relay") -> 2
            it.name.contains("typ srflx") -> 3
            else -> 4
        }}

        mDiagnosticExpandableAdapter?.addDiagnosticChildItemData(
            ExpandableDiagnosticModel(
                ExpandableDiagnosticModel.PARENT,
                DiagnosticData.DiagnosticParent(
                    PARENT_ICE_CANDIDATES_RECEIVED,
                    mDiagnosticIceCandidateReceived
                ),
                isExpanded = false,
                isCloseShown = true
            )
        )
    }
}