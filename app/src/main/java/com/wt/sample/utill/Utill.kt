package com.wt.sample.utill

import android.annotation.SuppressLint
import android.content.Context
import android.widget.Toast
import com.wt.sample.R
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

fun showError(context: Context, errorMessage: String?) {
    showToast(context, errorMessage, android.R.color.holo_red_dark)
}

fun showMessage(context: Context, errorMessage: String?) {
    showToast(context, errorMessage, R.color.colorAccent)
}

private fun showToast(context: Context, errorMessage: String?, textColorId: Int) {
    val t = Toast.makeText(context, errorMessage, Toast.LENGTH_LONG)
    t.show()
}

fun formatDurationToHHMMSSSSS(duration: Long): String? {
    val seconds = TimeUnit.MILLISECONDS.toSeconds(duration)
    val remainingSeconds = Math.abs(seconds % 60)
    val mins = seconds / 60
    val remainingMins = Math.abs(mins % 60)
    val hours = mins / 60
    return String.format(
        Locale.US,
        "%d:%02d:%02d",
        hours,
        remainingMins,
        remainingSeconds
    )
}

@SuppressLint("SimpleDateFormat")
fun getDate(milliSeconds: Long): String {
    // Create a DateFormatter object for displaying date in specified format.
    val formatter = SimpleDateFormat("hh:mm")
    // Create a calendar object that will convert the date and time value in milliseconds to date.
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = milliSeconds
    return formatter.format(calendar.time)
}