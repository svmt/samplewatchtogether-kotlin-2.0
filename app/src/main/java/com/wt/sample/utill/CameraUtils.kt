package com.wt.sample.utill

import android.content.Context
import android.graphics.SurfaceTexture
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraManager
import com.wt.sample.data.DiagnosticData


fun checkVideoResolutions(
    context: Context,
    diagnosticCamera: MutableList<DiagnosticData.DiagnosticParent.DiagnosticChild>
) {
    val manager = context.getSystemService(Context.CAMERA_SERVICE) as CameraManager
    for (id in manager.cameraIdList) {
        if (id == "2") {
            break
        }

        val characteristics = manager.getCameraCharacteristics(id)
        val map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)

        val isFrontCamera =
            characteristics.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_FRONT
        val cameraName = if (isFrontCamera) "Front" else "Back"
        diagnosticCamera.add(
            DiagnosticData.DiagnosticParent.DiagnosticChild(
                true,
                "Camera support $cameraName $id"
            )
        )

        if (map != null) {
            for (size in map.getOutputSizes(SurfaceTexture::class.java)) {
                if (size.width == 320 && size.height == 240 || size.width == 640 && size.height == 480 ||
                    size.width == 1280 && size.height == 720 || size.width == 1920 && size.height == 1080 ||
                    size.width == 2560 && size.height == 1440 || size.width == 3840 && size.height == 2160
                ) {
                    diagnosticCamera.add(
                        DiagnosticData.DiagnosticParent.DiagnosticChild(
                            true,
                            "Check resolution $size (${size.height}p)"
                        )
                    )
                }
            }
        }
    }
}